proxmox link : https://192.168.42.73:8006

Login dengan ldap username & ldap password
Realm diganti dengan RKS-NET

 ![Tux, the Linux mascot](img/Capture-pve.PNG)

Konfigurasi proxmox 

| kelompok     | network device - bridge | VMID  | hostname | ip addres     |
|--------------|-------------------------|-------|----------|---------------|
| 2 RKS B - 01 | vmbr201                 | VM211 | node211  | 172.16.201.11 |
|              |                         | VM212 | node212  | 172.16.201.12 |
| 2 RKS B - 02 | vmbr202                 | VM221 | node221  | 172.16.202.11 |
|              |                         | VM222 | node222  | 172.16.202.12 |
| 2 RKS B - 03 | vmbr203                 | VM231 | node231  | 172.16.203.11 |
|              |                         | VM232 | node232  | 172.16.203.12 |
| 2 RKS B - 04 | vmbr204                 | VM241 | node241  | 172.16.204.11 |
|              |                         | VM242 | node242  | 172.16.204.12 |
| 2 RKS B - 05 | vmbr205                 | VM251 | node251  | 172.16.205.11 |
|              |                         | VM252 | node252  | 172.16.205.12 |

Daftar kelompok 2 RKS B
| kelompok     | anggota                  | ldap username      | ldap password |
|--------------|--------------------------|--------------------|---------------|
| 2 RKS B - 01 | Ahtsani Nur Falah        | ahtsani.nur        | rks123456     |
|              | Ilham Aditya Nugraha     | ilham.aditya       | rks123456     |
|              | Auriel Delia             | auriel.delia       | rks123456     |
|              | Sari Dewi Ramadani       | sari.dewi          | rks123456     |
| 2 RKS B - 02 | Ditya Ardiansyah         | ditya.ardiansyah   | rks123456     |
|              | Kurnia Febrianto         | kurnia.febrianto   | rks123456     |
|              | Rachelia Afifah          | rachelia.afifah    | rks123456     |
|              | Gerry Raditya            | gerry.raditya      | rks123456     |
| 2 RKS B - 03 | Raissa Mumtaz            | raissa.mumtaz      | rks123456     |
|              | Muhammad Darmansyah Faiz | muhammad.darmasyah | rks123456     |
|              | Khalifah Wida            | khalifah.wida      | rks123456     |
|              | Marnilan Ayudiani        | marnilan.ayudiani  | rks123456     |
| 2 RKS B - 04 | Kiko Wahyudi             | kiko.wahyudi       | rks123456     |
|              | Muhammad Rizky           | muhammad.rizky     | rks123456     |
|              | Taqiya Nabilla           | taqiya.nabilla     | rks123456     |
|              | Sinta Permata Sari       | sinta.permata      | rks123456     |
| 2 RKS B - 05 | Putri Salsabila          | putri.salsabila    | rks123456     |
|              | Assyarif Rahman          | assyarif.rahman    | rks123456     |
|              | Muchammad Zhorif Teges   | muchammad.zhorif   | rks123456     |
|              | Adelia Fatma             | adellia.fatma      | rks123456     |
