1. Persiapan VM Client (Individu) 
2. Operasi Linux Dasar di Client
3. Remote Access & Virtualisasi (Kelompok)
   - Login ke Server Proxmox (sesuai username & password)
   - Buka VM console, login jika diperlukan (**ubuntu**:**ubuntu**) 
   - Cek VMID, IP Address, hostname, host sesuai dengan tabel di tutorial
   - Cek koneksi 2 node glusterfs
   - Cek paket software yang diperlukan/diinstall (glusterfs)
   - Cek Konfigurasi glusterfs
   - Modifikasi konfigurasi glusterfs
   - Uji replikasi glusterfs
      
4. Laporan Kelompok
