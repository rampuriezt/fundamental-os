AIM:
To study about the basics of UNIX

UNIX:
It is a multi-user operating system. Developed at AT & T Bell Industries, USA in 1969.
Ken Thomson along with Dennis Ritchie developed it from MULTICS (Multiplexed
Information and Computing Service) OS. By1980, UNIX had been completely rewritten using C language.

LINUX:
It is similar to UNIX, which is created by Linus Torualds. All UNIX commands works
in Linux. Linux is a open source software. The main feature of Linux is coexisting with other OS such as windows and UNIX.



1.   Fundamental Linux 
1.1. Basic Linux Command
1.1.1. File Handling Utilities

Cat Command: cat linux command concatenates files and print it on the standard output.

To Create a new file:

$ cat > file1.txt

This command creates a new file file1.txt. After typing into the file press control+d(^d) simultaneously to end the file.

To Append data into the file: To append data into the same file use append operator >> to write into thefile, else the file will be overwritten (i.e., all of its contents will be erased).

$ cat >> file1.txt

To display a file: This command displays the data in the file.cat file1.txt
To concatenate several files and display:
$ cat file1.txt file2.txt

The above cat command will concatenate the two files (file1.txt and file2.txt) and it will display the output in the screen. Some times the output may not fit the monitor screen. In such situation you canprint those files in a new file or display the file using less command.
$ cat file1.txt file2.txt | less

To concatenate several files and to transfer the output to anotherfile.
cat file1.txt file2.txt > file3.txt

In the above example the output is redirected to new file file3.txt.

rm COMMAND:
rm linux command is used to remove/delete the file from the directory.
To Remove / Delete a file: Here rm command will remove/delete the file file1.txt.rm file1.txt

To delete a directory tree:
$ rm -ir tmp

This rm command recursively removes the contents of all subdirectories of the tmp directory,prompting you regarding the removal of each file, and then removes the tmpdirectory itself.
To remove more files at once: rm command removes file1.txt and file2.txt files at the same time.
$ rm file1.txt file2.txt

cd COMMAND: cd command is used to change the directory.
cd linux-command
This command will take you to the sub-directory(linux-command) from its parent directory.

$ cd ..

This will change to the parent-directory from the current working directory/sub-directory.

$ cd ~

This command will move to the user's home directory which is "/home/username".
cp COMMAND:
cp command copy files from one location to another. If the destination is an existing file, then the file is overwritten; if the destination is an existing directory, the file is copied into the directory (the directory is not overwritten).
Copy two files:

$ cp file1.txt file2.txt

The above cp command copies the content of file1.txt to file2.txt

ls COMMAND:
ls command lists the files and directories under current working directory. Display root directorycontents:

$ ls /

lists the contents of root directory. Display hidden files and directories:

$ ls -a

lists all entries including hidden files and directories. Display inode information:

$ ls –i

ln COMMAND:
ln command is used to create link to a file (or) directory. It helps to provide soft link for desired files. Inode will be different for source and destination.

$ ln -s file1.txt file2.txt

Creates a symbolic link to 'file1.txt' with the name of 'file2.txt'. Here inode for 'file1.txt' and 'file2.txt'will be different.

mkdir command: Use this command to create one or more new directories.
Include one or more instances of the “<DIRECTORY” variable (separating each with a whitespace), and set each to the complete path to the new directory to be created.

mkdir OPTION <DIRECTORY>

rmdir command:

mv command:

diff command:

comm command:

wc command:


1.2. Shells
    - Identifying the Shell
    $ 
    $ bash --version

    - Shell Scripts
    
    - Positional Parameters
	- Input & Output
	- Doing Math
	- Exit Status
	- Comparisons with test
	- Conditional Statements
	- Flow Control: case
	- The borne for-Loop
	- The while and until Loops

1.3. Remote Access
     - SSH
     - FTP

1.4. Process Management
    - What is a Process?
    - Process Lifecycle
    - Process States
    - Viewing Processes
    ps Command:
    ps command is used to report the process status. ps is the short name for Process Status.
    List the current running processes.

    $ ps 

    Displays full information about currently running processes.

    $ ps -f

    kill COMMAND: kill command is used to kill the background process.

    Step by Step process:
    • Open a process music player or any file.xmms 
      press ctrl+z to stop the process.
    • To know group id or job id of the background task.jobs -l
      It will list the background jobs with its job id as,
    • xmms 3956
    • kmail 3467
    
    To kill a job or process.
    $ kill 3956
    kill command kills or terminates the background process xmms.



    - Signals
    - Tools to Send Signals
    - nohup and disown
    - Managing Processes
    - Tuning Process Scheduling
    - Job Control Overview
    - Job Control Commands
    - Persistent Shell Sessions with tmux
    - Persistent Shell Sessions with Screen
    - Using screen
    - Advanced Screen

    ----
PROCESS UTILITIES:
ps Command:
ps command is used to report the process status. ps is the short name for Process Status.
1. ps: List the current running processes.
Output:
PID TTY TIME CMD
2540 pts/1 00:00:00 bash
2. ps –f : Displays full information about currently running processes.
Output:
UID PID PPID C STIME TTY TIME CMD
nirmala 2540 2536 0 15:31 pts/1 00:00:00 bash
3. kill COMMAND: kill command is used to kill the background process.
Step by Step process:
• Open a process music player or any file.xmms
press ctrl+z to stop the process.
• To know group id or job id of the background task.jobs -l
It will list the background jobs with its job id as,
• xmms 3956
• kmail 3467
To kill a job or process.
• kill 3956
kill command kills or terminates the background process xmms.
Disk utilities:
du (abbreviated from disk usage) is a standard Unix program used to estimate file spaceusage—space used
under a particular directory or files on a file system.
$du kt.txt pt.txt /* the first column displayed the file's disk usage */
8 kt.txt
4 pt.txt
Using -h option: As mentioned above, -h option is used to produce the output in humanreadable format.
$du -h kt.txt pt.txt
8.0K kt.txt4.0K pt.txt
/*now the output is in human readable format i.e in Kilobytes */
Using -a option
$du -a kartik
8 kartik/kt.txt
4 kartik/pt.txt
4
kartik/pranjal.
/*so with -a option used all the files (under directory kartik) disk usage info is displayed alongwith
the thakral sub-directory */
df command : Report file system disk space usage
$df kt.txt
Filesystem 1K-blocks Used Available Use% Mounted on
/dev/the2 1957124 1512 1955612 1% /snap/core
/* the df only showed the disk usage details of the file system that contains file kt.txt */
//using df without any filename //
$df
/* in this case df displayed the disk usage details of all mounted file systems */
Using -h : This is used to make df command display the output in human-readable format.
//using -h with df//
$df -h kt.txt
Filesystem 1K-blocks Used Available Use% Mounted on
/dev/the2 1.9G 1.5M 1.9G 1% /snap/core
/*this output is easily understandable by the user and all cause of -h option */



NETWORKING COMMANDS
ping
The ping command sends an echo request to a host available on the network. Using this
command, you can check if your remote host is responding well or not.
Syntax: $ping hostname or ip-address
The above command starts printing a response after every second. To come out of thecommand,
you can terminate it by pressing CNTRL + C keys.
$ping google.com
PING google.com (74.125.67.100) 56(84) bytes of data.
64 bytes from 74.125.67.100: icmp_seq=1 ttl=54 time=39.4 ms
ftp: ftp stands for File Transfer Protocol. This utility helps you upload and download your filefrom
one computer to another computer.
Syntax $ftp hostname or ip-address
$ftp amrood.com
Connected to amrood.com.
220 amrood.com FTP server (Ver 4.9 Thu Sep 2 20:35:07 CDT 2009)Name (amrood.com:amrood):
amrood
331 Password required for amrood.Password:
230 User amrood logged in.ftp> dir
200 PORT command successful.
….
ftp> quit
221 Goodbye.
telnet:
Telnet is a utility that allows a computer user at one site to make a connection, login and then
conduct work on a computer at another site. Once you login using Telnet, you can perform all the
activities on your remotely connected machine.

C:>telnet amrood.comTrying...
Connected to amrood.com.Escape character is '^]'. login: amrood
amrood's Password:
******************************************************WELCOME TO AMROOD.COM *
*****************************************************
$ logoutLINUX PROGRAMMING LAB021-2022
Connection closed.C:>
Finger:
The finger command displays information about users on a given host. The host can be either
local or remote.
Check all the logged-in users on the local machine −
$ finger
Login Name Tty Idle Login Time Office
amrood pts/0 Jun 25 08:03 (62.61.164.115)
Check all the logged-in users on the remote machine –
$ finger @avtar.com
Login Name Tty Idle Login Time Office amrood pts/0 Jun 25 08:03 (62.61.164.115)
Get the information about a specific user available on the remote machine −
$ finger amrood@avtar.com
Ifconfig: Ifconfig is used to configure the network interfaces.

FILTERS
more COMMAND:
more command is used to display text in the terminal screen. It allows only backwardmovement.
1. more -c index.txt
Clears the screen before printing the file .
2. more -3 index.txt
Prints first three lines of the given file. Press Enter to display the file line by line.
head COMMAND:
head command is used to display the first ten lines of a file, and also specifies how many linesto
display.
1. head index.php
This command prints the first 10 lines of 'index.php'.
2. head -5 index.php
The head command displays the first 5 lines of 'index.php'.
3. head -c 5 index.php
The above command displays the first 5 characters of 'index.php'.
tail COMMAND:
tail command is used to display the last or bottom part of the file. By default it displays last10
lines of a file.
1. tail index.php
It displays the last 10 lines of 'index.php'.
2. tail -2 index.php
It displays the last 2 lines of 'index.php'.

3. tail -n 5 index.php
It displays the last 5 lines of 'index.php'.
4. tail -c 5 index.php
It displays the last 5 characters of 'index.php'.
cut COMMAND:
cut command is used to cut out selected fields of each line of a file. The cut command uses
delimiters to determine where to split fields.
cut -c1-3 text.txt
Output:
Thi
Cut the first three letters from the above line.
paste COMMAND:
paste command is used to paste the content from one file to another file. It is also used to set
column format for each line.
paste test.txt>test1.txt
Paste the content from 'test.txt' file to 'test1.txt' file.
sort COMMAND:
sort command is used to sort the lines in a text file.
1. sort test.txt
Sorts the 'test.txt'file and prints result in the screen.
2. sort -r test.txt
Sorts the 'test.txt' file in reverse order and prints result in the screen.
uniq
Report or filter out repeated lines in a file.
uniq myfile1.txt > myfile2.txt - Removes duplicate lines in the first file1.txt and outputs the
results to the second file.
TEXT PROCESSING UTILITIES
echo: display a line of text or echo command prints the given input string to standard output.eg.
echo I love India
echo $HOME
wc: print the number of newlines, words, and bytes in fileseg. wc file1.txt
nl: which lets you number lines in files.
eg. $ nl file11 hi
join- Join command is used for merging the lines of different sorted files based on the presence
of common field into a single line. The second line will be appended at the end ofthe first line
and cursor is placed at the end of line after joining.
Grep (Global Regular Expression Searching for a pattern), fgrep and egrep
$ grep ―sales director‖ emp1 emp2
$fgrep ‗good bad great‘ userfile
$egrep ‗good | bad | great‘ userfile

cat, head, tail, sort, uniq, cut, paste and etc.
BACKUP UTILITIES
Linux backup and restore can be done using backup commands tar, cpio, dump and restore.
Backup Restore using tar command
tar: tape archive is used for single or multiple files backup and restore on/from a tape or file.
$tar cvf /dev/rmt/0 *
Options: c -> create ; v -> Verbose ; f->file or archive device ; * -> all files and directories .
$tar cvf /home/backup *
Create a tar called backup in home directory, from all file and directories s in the currentdirectory.
Viewing a tar backup on a tape or file
$tar tvf /dev/rmt/0 ## view files backed up on a tape device.
$tar tvf /home/backup ## view files backed up inside the backup
Note: t option is used to see the table of content in a tar file.
Extracting tar backup from the tape
$tar xvf /home/backup ## extract / restore files in to current directory.
Note : x option is used to extract the files from tar file. Restoration will go to present directoryor
original backup path depending on relative or absolute path names used for backup.
Backup restore using cpio command
Using cpio command to backup all the files in current directory to tape.
find . -depth -print | cpio -ovcB > /dev/rmt/0
cpio expects a list of files and find command provides the list, cpio has to put these file onsome
destination and a > sign redirect these files to tape. This can be a file as well .
Viewing cpio files on a tape
cpio -ivtB < /dev/rmt/0
## Options i -> input ; v->verbose; t-table of content; B-> set I/O block size to 5120 bytes
Restoring a cpio backup
cpio -ivcB < /dev/rmt/0
## Options i -> input ; v->verbose; t-table of content; B-> set I/O block size to 5120 bytes


Task 



