# Catatan :
penamaan node01 dan node01 sesuai dengan pembagian kelompok, 

Pada kelas 2 RKS A kelompok 1 sebagai berikut :

node01 merupakan virtua mesin dengan hostname **node111 (node111.rks.net)**

node02 merupakan virtua mesin dengan hostname **node112 (node112.rks.net)**

pada kelas 2 RKS B kelompok 1

node01 merupakan virtual mesin dengan hostname **node211 (node211.rks.net)**

node02 merupakan virtual mesin dengan hostname **node212 (node212.rks.net)**


# Server Mirror 1 (node01)
1. Check hostname
``` 
$ cat /etc/hostname
$ cat /etc/hosts
```
2. check koneksi ke masing-masing node
```
$ ping node01

$ ping node02
```
3. Check service glusterd berjalan
```
$ sudo systemctl status glusterd


$ sudo systemctl status glusterd | grep running

```
apabila service belum berjalan, start glusterd service

```
$ sudo systemctl start glusterd
```
4. check status peer glusterfs

```
$ sudo gluster peer status

$ sudo gluster peer probe node02

```
apabila belum bisa terkoneksi ulangi langkah 2 dan 3 di node02

5. Membuat partisi/volume yang akan disinkronkan
   nama volume : **mirror**
   path volume : **node01:/glusterfs/vols**
   ```
   $ sudo mkdir -p /glusterfs/vols
   
   $ cd /glusterfs
   
   $ ls 

   ```
   direktori untuk volume dibuat pada node01 dan node02.

   langkah berikutnya membuat glusterfs volume dengan direktori yang telah ada

   ```
   sudo glusterfs volume create mirror replica 2 node01:/glusterfs/vols node02:/glusterfs/vols force
   
   ```

   Output jika berhasil :
   ```
   volume create: mirror: success: please start the volume to access data
   ```

   setelah volume berhasil dibuat, jalankan volume tersebut

   ```
   sudo glusterfs start mirror

   ```
   Output jika berhasil
   ```
   volume start: mirror: success
   ```

   lakukan pengecekan status volume

   ```
   $ sudo gluster volume status
   ```
   Output jika berhasil

```
Status of volume: mirror
Gluster process                             TCP Port  RDMA Port  Online  Pid
------------------------------------------------------------------------------
Brick node01.rks.net:/gluster/vols          49152     0          Y       18801
Brick node02.rks.net:/gluster/vols          49152     0          Y       19028
Self-heal Daemon on localhost               N/A      N/A        Y       19049
Self-heal Daemon on node02.rks.net          N/A      N/A        Y       18822
 
Task Status of Volume mirror
------------------------------------------------------------------------------ 
There are no active volume tasks
```

6. Memulai replikasi gluster
persiapkan direktori/folder yang akan di sinkronkan, 
dalam hal ini dibuat folder pada **/home/ubuntu/shared** di masing-masing node

```
$ mkdir -p /home/ubuntu/shared

$ cd /home/ubuntu

$ ls

```

lakukan mounting pada masing-masing node
```
sudo mount -t glusterfs node01:/mirror /home/ubuntu/shared
```

lakukan uji membuat file pada direktori /home/ubuntu/shared

```
$ sudo touch /home/ubuntu/shared/test.txt

$ ls /home/ubuntu/shared
```

check file di node lain

```
$ ls /home/ubuntu/shared

```

buat beberapa file yang berisi nama dan npm, lakukan cross check antar node


Setelah Mounting berhasil dibuat, agar ketika terdapat server down pada salah satu cluster, Maka solusinya adalah menambahkan blok gluster pada fstab, agar ketika server kembali UP, peer akan otomatis tersambung kembali.

```
$ sudo nano /etc/fstab
```
tambahkan pada baris terakhir

pada node01:
```
# Server node 1 :
node01:/mirror /home/ubuntu/shared glusterfs defaults,_netdev 0 0
```

pada node02:
```
# Server node 1 :
node02:/mirror /home/ubuntu/shared glusterfs defaults,_netdev 0 0
```


lakukan pengujian dengan mematikan salah satu node, menambahkan file dan menghidupkan node kembali.
pastikan file direplikasi pada setiap node 





# Server Mirror 2 (node02)

1. Check hostname
``` 
$ cat /etc/hostname
$ cat /etc/hosts
```
2. check koneksi ke masing-masing node
```
$ ping node01

$ ping node02
```
3. Check service glusterd berjalan
```
$ sudo systemctl status glusterd


$ sudo systemctl status glusterd | grep running

```
apabila service belum berjalan, start glusterd service

```
$ sudo systemctl start glusterd
```
4. check status peer glusterfs

```
$ sudo gluster peer status

$ sudo gluster peer probe node02

```
apabila belum bisa terkoneksi ulangi langkah 2 dan 3 di node02

5. Membuat partisi/volume yang akan disinkronkan
   nama volume : ** mirror **
   path volume : ** node01:/glusterfs/vols
   ```
   $ sudo mkdir -p /glusterfs/vols
   
   $ cd /glusterfs
   
   $ ls 

   ```
   direktori untuk volume dibuat pada node01 dan node02.

   langkah berikutnya membuat glusterfs volume dengan direktori yang telah ada

   ```
   sudo glusterfs volume create mirror replica 2 node01:/glusterfs/vols node02:/glusterfs/vols force
   
   ```

   Output jika berhasil :
   ```
   volume create: mirror: success: please start the volume to access data
   ```

   setelah volume berhasil dibuat, jalankan volume tersebut

   ```
   sudo glusterfs start mirror

   ```
   Output jika berhasil
   ```
   volume start: mirror: success
   ```

   lakukan pengecekan status volume

   ```
   $ sudo gluster volume status
   ```
   Output jika berhasil

```
Status of volume: mirror
Gluster process                             TCP Port  RDMA Port  Online  Pid
------------------------------------------------------------------------------
Brick node01.rks.net:/gluster/vols          49152     0          Y       18801
Brick node02.rks.net:/gluster/vols          49152     0          Y       19028
Self-heal Daemon on localhost               N/A      N/A        Y       19049
Self-heal Daemon on node02.rks.net          N/A      N/A        Y       18822
 
Task Status of Volume mirror
------------------------------------------------------------------------------ 
There are no active volume tasks
```

6. Memulai replikasi gluster
persiapkan direktori/folder yang akan di sinkronkan, 
dalam hal ini dibuat folder pada **/home/ubuntu/shared** di masing-masing node

```
$ mkdir -p /home/ubuntu/shared

$ cd /home/ubuntu

$ ls

```

lakukan mounting pada masing-masing node
```
sudo mount -t glusterfs node01:/mirror /home/ubuntu/shared
```

lakukan uji membuat file pada direktori /home/ubuntu/shared

```
$ sudo touch /home/ubuntu/shared/test.txt

$ ls /home/ubuntu/shared
```

check file di node lain

```
$ ls /home/ubuntu/shared

```

buat beberapa file yang berisi nama dan npm, lakukan cross check antar node


Setelah Mounting berhasil dibuat, agar ketika terdapat server down pada salah satu cluster, Maka solusinya adalah menambahkan blok gluster pada fstab, agar ketika server kembali UP, peer akan otomatis tersambung kembali.

```
$ sudo nano /etc/fstab
```
tambahkan pada baris terakhir

pada node01:
```
# Server node 1 :
node01:/mirror /home/ubuntu/shared glusterfs defaults,_netdev 0 0
```

pada node02:
```
# Server node 1 :
node02:/mirror /home/ubuntu/shared glusterfs defaults,_netdev 0 0
```
