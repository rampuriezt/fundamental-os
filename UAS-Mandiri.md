# Pengambilan File Log dari VM ubuntu/Linux (File OVA tugas ).

- Jalankan VM yang telah diberikan sejak tanggal 23 January 2024 (OVA dalam USB)
- Pastikan Network di NAT 
- cek ip address
```
$ ip address

```
default untuk virtualbox NAT ada di ip 10.0.2.15

- pastikan Service SSH berjalan 
```
$ sudo systemctl status ssh
```
- buka setting Network, konfigurasi port forward
 ![Tux, the Linux mascot](img/NAT-Port-Forward.png)

- download dan install winscp https://winscp.net/eng/downloads.php

- buat site baru dan arahkan ke ssh di vm Linux
 ![Tux, the Linux mascot](img/winscp.png)

- arahkan ke folder /tmp yang telah dibuat terdapat archive file-file log

- download file archive tersebut

- upload di LMS
 

