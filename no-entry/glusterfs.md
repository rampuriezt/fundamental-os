**TBD Weeks 2**
# Daftar Isi
1. Apa itu Network File System?
2. Pengertian GlusterFS
3. Cara Install GlusterFS pada Ubuntu 20.04
- Step 1. Persiapan Sistem
- Step 2. Konfigurasi DNS/FileHost
- Step 3. Install Package GlusterFS
- Step 4. Konfigurasi Firewall dan Peer pada Gluster
- Step 5. Membuat partisi/volume yang akan disinkronkan
- Step 6. Memulai replikasi gluster
4. Pengujian dan Pengecekan Status Gluster

Sebelum kita membahas tentang GlusterFS, terlebih dahulu kami akan membahas tentang NFS yang berfungsi membagi data ke seluruh server node untuk memastikan bahwa data terdistribusikan. NFS merupakan sistem yang juga digunakan oleh GlusterFS.

# Apa itu Network File System?

Network File System atau yang disingkat menjadi NFS adalah sebuah protokol yang berfungsi untuk membagikan data melalui jaringan. NFS akan membagikan file maupun resource melalui network atau jaringan, tanpa peduli sistem operasi yang digunakan apa.

NFS bekerja dengan membagi data ke seluruh server-server node yang dimiliki, dan memastikan bahwa semua data yang ada di server satu ada di server lainnya tanpa ada potensi kehilangan data.

# Pengertian GlusterFS

GlusterFS adalah salah satu pengembangan dari ekosistem NFS yang memiliki fungsi serupa sebagai shared storage. Namun, GlusterFS memiliki beberapa perbedaan signifikan. Berbeda dengan NFS yang menggunakan protokol berbasis client-server, GlusterFS mengadopsi sistem penggabungan penyimpanan antara server menjadi satu ruang penyimpanan tunggal yang dapat diakses oleh klien.

GlusterFS adalah sebuah sistem file terdistribusi open-source yang dirancang untuk mengelola dan menyimpan data secara terdistribusi melalui beberapa server dalam sebuah jaringan. Selain itu, GlusterFS merupakan solusi penyimpanan berkas yang sangat scalable dan dapat disesuaikan dengan kebutuhan Anda.

Distributed Glusterfs Volume - install Glusterfs Distributed Glusterfs Volume
GlusterFS beroperasi dengan menggabungkan kapasitas penyimpanan dari beberapa server fisik atau virtual menjadi satu entitas penyimpanan tunggal yang disebut “volume.”

Volume ini dapat diakses oleh klien dari berbagai lokasi dalam jaringan. Salah satu fitur utama dari GlusterFS adalah kemampuannya untuk secara otomatis menyebarkan dan mereplikasi data melalui server-server tersebut, sehingga meningkatkan ketersediaan data dan toleransi terhadap kesalahan.

# Cara Install GlusterFS pada Ubuntu 20.04

Paket GlusterFS dapat berjalan dengan baik pada OS Ubuntu 20.04 dan CentOS 7, Dikarenakan sistem sharing data pada gluster tidak menggunakan enkripsi, kami sarankan melakukan instalasi gluster pada jaringan internal VPS atau dapat menggunakan firewall untuk membatasi akses.

- Step 1. Persiapan Sistem
Berikut beberapa persiapan sistem yang diperlukan untuk install GlusterFS pada server.
server linux dengan OS Ubuntu 20.04.
Minimal 2GB Ram, dengan ip public statis
Domain dan Subdomain
Firewall UFW / CSF / Firewalld

- Step 2. Konfigurasi DNS/FileHost
Langkah pertama untuk install GlusterFs adalah kita perlu melakukan konfigurasi pada dns/domain dengan statis. Hal ini diperlukan untuk pointing gluster pada masing masing IP server.

Pada Lab ini, kita akan membuat subdomain bernama mirror pada ke-2 IP address VPS sebagai berikut :

subdomain : mirror1.rks.com
ip address  : 172.16.10.21
subdomain : mirror2.rks.com
ip address  : 172.16.10.22
Lakukan penyesuaian host pada konfigurasi filehost yaitu file /etc/hosts di kedua VPS.
```
$ sudo nano /etc/hosts

127.0.0.1       localhost
172.16.10.21   mirror1.rksa gluster1
172.16.10.22   mirror2.rksa gluster2
```
Simpan konfigurasi file dan pastikan saat melakukan ping ke gluster2 dari VPS pertama akan mereply ke ip yang benar.
```
$ ping gluster2
```

- Step 3. Install Package GlusterFS
Selanjutnya, Kita akan melakukan instalasi dan aktivasi pada paket glusterfs di masing masing server VPS.

```
sudo add-apt-repository ppa:gluster/glusterfs-7

sudo apt update

sudo apt install glusterfs-server
```
Aktifkan package gluster yang telah terinstall melalui systemd pada masing” server.
```
$ sudo systemctl start glusterd.service
$ sudo systemctl enable glusterd.service
$ sudo systemctl status glusterd.service
```
Pastikan gluster dapat aktif dan berjalan pada VPS.

- Step 4. Konfigurasi Firewall dan Peer pada Gluster
Tambahkan ip host VPS 2 ke Ip Vps 1 begitupun sebaliknya, Pastikan hanya allow untuk ip tersebut yang dapat mengakses port default Gluster yaitu 24007, berikut kami contohkan jika menggunakan UFW

Server Mirror 1 :
```
sudo ufw allow from 172.16.10.21 to any port 24007
```

Server Mirror 2 :
```
$ sudo ufw allow from 172.16.10.22 to any port 24007
```
Untuk memastikan tidak ada server lain yang bisa mengakses node, kita deny semua incoming/outgoing selain ip mirror dengan command berikut :
```
$ sudo ufw deny 24007
```
Apabila Firewall telah ditambahkan, lakukan restart pada service ufw
```
$ systemctl restart ufw
$ ufw reload
```
Langkah selanjutnya adalah menambahkan menghubungkan antar node dari Gluster

Server Mirror 1 :
```
$ sudo gluster peer probe gluster2
```

Server Mirror 2 :

```
$ sudo gluster peer probe gluster1
```

Melakukan pengecekan Peering :
```
$ sudo gluster peer status
Output
Number of Peers: 1
Hostname: mirror2.rks.com
Uuid: b98ae496-c4eb-4b20-9729e-7840230407be
State: Peer in Cluster (Connected)
```

- Step 5. Membuat partisi/volume yang akan disinkronkan
Pada Lab ini kita akan membuat volume untuk sinkronisasi file dan data, study kasus Lab pada case untuk backend load balancer, maka kita akan melakukan sinkronisasi file dari server 1 ke 2 dan 2 ke 1 pada direktori website, yaitu /var/www/html/website

Untuk membuat volume pada Gluster, gunakan command berikut ini, sesuaikan dengan path direktori website.

sudo gluster volume create nama_volume replica jumlah_node namadomain1.com:/lokasi/volume/gluster namadomain2.com:/lokasi/volume/gluster force
Penjelasan : 

nama_volume : nama volume adalah nama yang akan digunakan sebagai status gluster, berikan nama bebas sesuai keinginan.
jumlah_node : isikan sesuai jumlah server gluster kita, jika ada 2, isikan dengan 2.
namadomain.com : digunakan untuk mendeskripsikan host, sebelumnya sudah membuat host bernama mirror1 dan 2.
force : mengabaikan semua pertimbangan atau error, jika di force tidak akan muncul informasi debug
Praktek :

$ sudo gluster volume create mirror replica 2 mirror1.eepisat.com:/gluster/volume/ mirror2.eepisat.com:/gluster/volume/ force
Output jika berhasil :
volume create: mirror: success: please start the volume to access data

Lakukan start pada gluster untuk memulai volume.
```
sudo gluster volume start mirror
Output
volume start: mirror: success
```
Selanjutnya dapat memeriksa status peer Gluster :
```
sudo gluster volume status
Output
Status of volume: volume1
Gluster process                             TCP Port  RDMA Port  Online  Pid
------------------------------------------------------------------------------
Brick mirror1.eepisat.com:/gluster/volume 49152     0          Y       18801
Brick mirror2.eepisat.com:/gluster/volume 49152     0          Y       19028
Self-heal Daemon on localhost              N/A      N/A        Y       19049
Self-heal Daemon on mirror1.eepisat.com    N/A      N/A        Y       18822
 
Task Status of Volume mirror
------------------------------------------------------------------------------ There are no active volume tasks
Berdasarkan informasi diatas, peer telah berhasil dilakukan, langkah selanjutnya kita dapat melakukan allow transfer data pada port 49152 default, dan kita harus allow juga pada port 49153 49154 sebagai 2 volume mirror.
```
Server Mirror 1 : 
```
$ sudo ufw allow from 203.175.8.224 to any port 49152
```
Server Mirror 2 : 
```
sudo ufw allow from 203.175.8.223 to any port 49152
```
Block semua akses selain 2 ip diatas dengan command :
```
sudo ufw deny 49152
```
- Step 6. Memulai replikasi gluster
Setelah membuat volume, langkah selanjutnya adalah dengan mounting volume yang telah dibuat sebelumnya dan diarahkan pada lokasi folder website/data yang akan disinkronkan. Pada contoh artikel kali ini, kami akan melakukan mounting pada /var/www/html/website

Server Mirror 1:

sudo mount -t glusterfs mirror1:/nama_volume /var/www/html/website
Server Mirror 2:

sudo mount -t glusterfs mirror2:/nama_volume /var/www/html/website
Catatan: nama volume diisi dengan yang sudah dibuat sebelumnya, jika pada panduan ini volume bernama mirror.

Setelah Mounting berhasil dibuat, agar ketika terdapat server down pada salah satu cluster, Maka solusinya adalah menambahkan blok gluster pada fstab, agar ketika server kembali UP, peer akan otomatis tersambung kembali.

sudo nano /etc/fstab
Server Mirror 1 :
mirror1:/mirror /var/www/html/website glusterfs defaults,_netdev 0 0

Server Mirror 2 :
mirror2:/mirror /var/www/html/website glusterfs defaults,_netdev 0 0

Pengujian dan Pengecekan Status Gluster
Untuk melakukan pemeriksaan status gluster, dapat menggunakan gluster info
```
sudo gluster volume info
Output

Volume Name: mirror
Type: Replicate
Volume ID: bac03075-a223-43ab-a0f6-6121115940b0c
Status: Started
Snapshot Count: 0
Number of Bricks: 1 x 2 = 2
Transport-type: tcp
Bricks:
Brick1: mirror1.eepisat.com:/gluster/volume
Brick2: mirror2.eepisat.com:/gluster/volume
Options Reconfigured:
transport.address-family: inet
storage.fips-mode-rchecksum: on
nfs.disable: on
performance.client-io-threads: off
```
Langkah selanjutnya kita dapat membuat sebuah file pada direktori /var/www/html/website dari server mirror 1 maka akan otomatis ada pada mirror 2 begitupun sebaliknya, apabila ada perubahan pada file dan data akan otomatis replikasi pada tujuan path.

Sampai tahap ini, install GlusterFS pada server telah selesai
