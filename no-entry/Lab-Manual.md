# List of Lab Experiments
1. Fundamental Linux (1 Week/Individual)
   - Basic Linux Command
   - Shells
   - Remote Access
   - Process Management
2. Virtualization (1 Week/Group)
   - VirtualBox
   - Proxmox
3. Distributed File System (1 Week/Group)
   - Network File System
   - GlusterFS
4. ~~Special Purpose OS~~


# Lab Preparation
1. Download VPN Clients
2. download lab credentials
3. Download lab OVA ()
4. Import OVA to VirtualBox 
5. Setup VM networking
6. start VM
7. login VM as user
8. Login VM as 'LDAP user'

# Notes:
- OVA username:passwod is **ubuntu:ubuntu**
- TBD

# Troubleshooting :
1. Cant login using LDAP account
   - check ip address

   ```
   $ ip address
   
   $ ip link
   ```
   - check ldap server
   ```
   $ ping ldap.rks.net
   ```
   - check account
