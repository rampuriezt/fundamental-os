proxmox link : https://192.168.42.72:8006

Login dengan ldap username & ldap password
Realm diganti dengan RKS-NET

 ![Tux, the Linux mascot](img/Capture-pve.PNG)

Konfigurasi proxmox 

| kelompok     | network device - bridge | VMID  | hostname | ip addres     |
|--------------|-------------------------|-------|----------|---------------|
| 2 RKS A - 01 | vmbr101                 | VM111 | node111  | 172.16.101.11 |
|              |                         | VM112 | node112  | 172.16.101.12 |
| 2 RKS A - 02 | vmbr102                 | VM121 | node121  | 172.16.102.11 |
|              |                         | VM122 | node122  | 172.16.102.12 |
| 2 RKS A - 03 | vmbr103                 | VM131 | node131  | 172.16.103.11 |
|              |                         | VM132 | node132  | 172.16.103.12 |
| 2 RKS A - 04 | vmbr104                 | VM141 | node141  | 172.16.104.11 |
|              |                         | VM142 | node142  | 172.16.104.12 |
| 2 RKS A - 05 | vmbr105                 | VM151 | node151  | 172.16.105.11 |
|              |                         | VM152 | node152  | 172.16.105.12 |

Kelompok 2 RKS A

| kelompok | nama                                 | ldap username         | ldap password |
|----------|--------------------------------------|-----------------------|---------------|
|2 RKS A - 01 | Respramon	Sollus Sihombing           | respramon.sollus	   | rks123456     |
|| Muhammad Galvin Fata	                   | muhammad.galvin	   | rks123456     |
|| Satwika Prabhawananda	               | satwika.prabhawananda | rks123456     |
|| Prilya Widay Erlani	                   | prilya.widay	       | rks123456     |
|2 RKS A - 02 | Arga Ariyuda Avian	                   | arga.ariyuda	       | rks123456     |
|| Samuel Partogian Dominggos Siregar   | samuel.partogian	   | rks123456     |
|| Muhammad Raihan Palevi	               | raihan.palevi	       | rks123456     |
|| Kevin	Armando Siburian 	           | kevin.armando	       | rks123456     |
|2 RKS A - 03 | Mufti	Hanif                          | mufti.hanif	       | rks123456     |
|| Fioren Fio Ermada	                   | fioren.fio	           | rks123456     |
|| Michael Brensco Siringo-Ringo	       | michael.brensco	   | rks123456     |
|| Andrian Natanael Sinaga	               | andrian.natanael	   | rks123456     |
|2 RKS A - 04 | A. Fakhrul Adani	                       | a.fakhrul	           | rks123456     |
|| Gabriella	Elisya Simanjuntak         | gabriella.elisya      | rks123456     |
|| Paul Uriel Partoguan Siburian	       | paul.uriel	           | rks123456     |
|| Gede Gangga Widiagung	               | gede.gangga	       | rks123456     |
|2 RKS A - 05 | Della	Yustina Panggabean	           | della.yustina	       | rks123456     |
|| Putu Arya Nirmala Gita Kirana	       | putu.arya	           | rks123456     |
|| Martua Raja Doli Pangaribuan            | martua.raja	       | rks123456     |
|| Kevin	Pangeran Enrico	               | kevin.pangeran	       | rks123456     |

