# Basic Linux Command
1. AIM :
- File Related Command
  - man

    used provide manual help on every linux command
    ```
    $ man cat
    ```
  - touch

    used to create blank file
    ```
    $ touch file_name.txt
    ```
  - cat

    used to display content of file
  - head

    used to diplay last n lines content of files, default number n = 10, option -n to diplay n number last lines
    ``` 
    $ head -n 2 filename.txt
    ```
  - tail

    used to diplay last n lines content of files, default number n = 10, option -n to diplay n number last lines 
    ```
    $ tail filename.txt
    ```
    to monitor file use option -f
    ```
    $ tail -f /var/log/syslog
    ```
    
  - more
    ```
    $ man cat | more
    ```
  - less
    ```
    $ man cat | less
    ```
  - uniq

    remove duplicate  
    ```
    $ cat try.txt
    try a linux command
    try a linux command
    try a linux command

    try a linux command everyday
    try a linux command everyday

    $ uniq try.txt
    try a linux command

    try a linux command everyday    
    ```

  - sort
    ```
    $ cat file.txt
    orange
    bluebeery
    apple
    banana

    $ sort file.txt
    apple
    banana
    blueberry
    orange

    ```
    
  - diff

    used to view file difference
    ```
    diff file1.txt file2.txt
    ```

  - wc

    used to display lines,word, character in a files with option -l, -w, -c
    ```
    $ wc -l file_name.txt 
    ```

  - chown
  - chmod
  - who

    used to diplay all user
  - whoami

    used to display all user who have logged to system
  - adduser
  
    used to display current user logged to system
  - useradd
  - usermod
  - userdel
  - passwd

- Text Editor Related
  - nano
  ``` 
  $ nano filename.txt
  ```
  use **CTRL+O** to overwrite file.

  use **CTRL+X** to exit from nano.
    
  - vi
  ```
  $ vi filename.txt
  ```
  **insert** to enter edit mode

  **ESC** to exit edit mode

  **:q** to exit

  **:wq** to overwrite file


- Directpry Related Command
  - la

    used to listing files and directory
    ```
    $ ls /home/ubuntu
    
    $ ls -la /var/log
    ```

  - mkdir

    used to create directory
  ```
  $ mkdir dir_name
  ```
  - cd

    used to chang directory
  ```
  $ cd dir_name
  ```
  - pwd
  ```
  $ pwd
  ```
- Network Related Command
  - ip
  ```
  $ ip

  $ ip a

  $ ip address

  $ ip link
  ```

  - hostname
    used to display, set unset hostname
  ```
  $ hostname -f
  ```

  - ping
  ```
  $ ping IP_ADDRESS
  ```
  - telnet
  ```
  $ telnet IP_ADDRESS PORT
  ```

  - ftp
  - ssh
  - wget

    used to download file
    ```
    $ wget http://domain.com/somefile.txt
    ```
  
- General Purpose Command
  - echo
  ``` 
  $ echo "this is text to display"
  ```

  - clear

    used to clear screen
    ````
    $ clear
    ```

  - date

    used to display date
    ```
    $ date
    ```

  - time
    
    used to display current time
    ``` 
    $ time
    ```

  - grep
  
- Backup Related Command
  - zip
  - unzip
  - tar

    used to create and extract TAR archive files
    
    archiving files
    ```
    $ tar cvf archive_file.tar filename.ext
    
    $ tar cvf archive_file.tar *.ext
    ```
    
    unarchive tar file
    ```
    $ tar xvf archivfile.tar

    ```

2. TASK :

# Linux System Calls

dir_simulation.c
```
#include<stdio.h>
#include<stdlib.h>
#include<dirent.h>

struct dirent *dptr;
int main(int argc, char *argv[])
{
char buff[100];
DIR *dirp;
printf("\n\n ENTER DIRECTORY NAME");
scanf("%s", buff);
if((dirp=opendir(buff))==NULL)
{
printf("The given directory does not exist");
exit(1);
}
while(dptr=readdir(dirp))
{
printf("%s\n",dptr->d_name);
}
closedir(dirp);
}
```
process.c
```
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
main()
{
int pid,pid1,pid2;
pid=fork();
if(pid==-1)
{
printf("ERROR IN PROCESS CREATION \n");
exit(1);
}
if(pid!=0)
{
pid1=getpid();
printf("\n the parent process ID is %d\n", pid1);
}
else
{
pid2=getpid();
printf("\n the child process ID is %d\n", pid2);
}
}
```

TASK
- compile

example compile
```
$ gcc dir_simulation.c -o dir_simulation

```
- execute
```
$ ./dir_simulation

```
- review

observe output, create txt file and write your observation 

# Linux Shell
sample of **sample.sh**

```
#!/bin/sh
echo " this is a sample bash script"
echo "whoami?"
whoami

echo "check IP_ADDRESS"
ip address
```
add execute permission
```
$ chmod +x sample.h
$ bash sample.sh
```

# Process Management
1. AIM

- ps
- kill
- jobs

2. TASK
TBD

# Memory Management
TBD

# TASK Lab (**update 1**)
  - try every command at least once succes, look at references for detailed information

  - add username with passowrd, then login using username

  - create directory **student**, enter the directory and create empty file with filename **student.txt**. after file cretae, editng empty file and fill 10 lines with students name, save the file. check content of file and try Command head, tail with 3 number of line. perform sort command to filtering ascending and descending alphabetically.
  - rename file student.txt to **students.txt**, change to **students**, rename the direcrtory to **students**, list conten of directory
  - archive **students** direcrtory, create new direcrtory **backup**, copy archived file to **backup** and unarchive

# References
  - https://www.geeksforgeeks.org/linux-commands/?ref=lbp
  
